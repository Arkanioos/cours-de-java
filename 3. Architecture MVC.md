<center>
    <img src="https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png" width="200"></td>
</center>

# Cours de Java - L'architecture MVC

<h2>Pluquet Frédéric, HELHa, Année académique 2023-2024</h2>

<h2>Sommaire</h2>

<!-- vscode-markdown-toc -->
* 1. [Introduction](#Introduction)
	* 1.1. [Le modèle](#Lemodle)
	* 1.2. [La vue](#Lavue)
	* 1.3. [Le contrôleur](#Lecontrleur)
	* 1.4. [Avantages](#Avantages)
	* 1.5. [Inconvénients](#Inconvnients)
	* 1.6. [Sources](#Sources)
* 2. [Qualité de code](#Qualitdecode)
	* 2.1. [Découplage](#Dcouplage)
	* 2.2. [Cohésion](#Cohsion)
	* 2.3. [Découplage et cohésion](#Dcouplageetcohsion)
* 3. [Première approche de l'architecture MVC](#PremireapprochedelarchitectureMVC)
	* 3.1. [Le modèle](#Lemodle-1)
	* 3.2. [La vue](#Lavue-1)
	* 3.3. [Le contrôleur (d'application)](#Lecontrleurdapplication)
* 4. [Gestion de différentes vues](#Gestiondediffrentesvues)
	* 4.1. [Découplage](#Dcouplage-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduction'></a>Introduction 

MVC signifie Modèle-Vue-Contrôleur. C'est une architecture logicielle qui sépare les données de l'interface utilisateur. Cela permet de séparer les différentes responsabilités de l'application.

![Architecture](images/Model-View-Controller_architectural_pattern-fr.svg)

###  1.1. <a name='Lemodle'></a>Le modèle

Le modèle représente les données de l'application. Il est responsable de la gestion des données. Il peut être composé de plusieurs classes.

###  1.2. <a name='Lavue'></a>La vue

La vue représente l'interface utilisateur. Elle est responsable de l'affichage des données. Elle peut être composée de plusieurs fichiers.

###  1.3. <a name='Lecontrleur'></a>Le contrôleur

Le contrôleur est responsable de la gestion des événements. Il est responsable de la communication entre le modèle et la vue.


###  1.4. <a name='Avantages'></a>Avantages

L'architecture MVC permet de séparer les différentes responsabilités de l'application. Cela permet de mieux organiser le code et de mieux le maintenir.

###  1.5. <a name='Inconvnients'></a>Inconvénients

L'architecture MVC peut être complexe à mettre en place et à comprendre. Cela peut prendre du temps pour bien la comprendre et l'appliquer. Mais cela en vaut la peine.

###  1.6. <a name='Sources'></a>Sources

* [Wikipedia](https://fr.wikipedia.org/wiki/Mod%C3%A8le-vue-contr%C3%B4leur)

##  2. <a name='Qualitdecode'></a>Qualité de code 

Avant de parler d'architecture, il faut parler de qualité de code. La qualité de code est très importante. Elle permet de savoir si le code est bien organisé et si il est facile à maintenir. Il existe plusieurs techniques pour mesurer la qualité de code. Nous allons voir deux d'entre elles : le découplage et la cohésion.

###  2.1. <a name='Dcouplage'></a>Découplage

- On dit que deux classes sont très couplées si elles dépendent l'une de l'autre. Cela signifie que si l'une des deux classes change, l'autre classe doit aussi changer. En revanche, si deux classes sont peu couplées, elles ne dépendent pas l'une de l'autre : si l'une des deux classes change, l'autre classe n'a donc plus besoin de changer.

- On peut calculer le couplage entre deux classes en calculant le nombre de méthodes/attributs qui sont utilisés par l'une des deux classes sur l'autre. Si ce nombre est élevé, les classes sont fortement couplées. Si ce nombre est faible, les classes sont peu couplées.

- Le découplage est donc une technique qui permet de séparer les différentes responsabilités d'une application dans des classes bien séparées les unes des autres. Cela permet de mieux organiser le code et de mieux le maintenir.


###  2.2. <a name='Cohsion'></a>Cohésion

- On dit qu'une classe est cohésive si elle a une seule responsabilité. Cela signifie que la classe ne fait qu'une seule chose. Si une classe a plusieurs responsabilités, on dit qu'elle est peu cohésive.


###  2.3. <a name='Dcouplageetcohsion'></a>Découplage et cohésion

- On dit qu'une classe est bien conçue si elle est peu couplée et cohésive. Cela signifie que la classe est bien organisée et qu'elle ne fait qu'une seule chose. Si une classe est mal conçue, elle est fortement couplée et/ou peu cohésive.

- Nous allons voir comment nous pouvons gérer le découplage et la cohésion dans notre application via une architecture bien pensée en MVC.


##  3. <a name='PremireapprochedelarchitectureMVC'></a>Première approche de l'architecture MVC

###  3.1. <a name='Lemodle-1'></a>Le modèle

- Voici un exemple simple d'un modèle ```Personne``` en Java :

```java
public class Personne {

    private String nom;
    private String prenom;
    private int age;

    public Personne(String nom, String prenom, int age) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
```

- On voit que le modèle ```Personne``` contient les attributs ```nom```, ```prenom``` et ```age```. Il contient aussi les méthodes ```get``` et ```set``` pour chacun de ces attributs. Cela permet de récupérer et de modifier les valeurs de ces attributs.
- Le modèle ne doit pas contenir de code spécifique à l'interface graphique. Il ne doit pas non plus contenir de code spécifique à la base de données. Il doit juste contenir les données et les méthodes pour les manipuler.

###  3.2. <a name='Lavue-1'></a>La vue

- Voici un exemple simple d'une vue ```PersonneView``` en Java :

```java
public class PersonneViewConsole {

    private Personne model;

    // le constructeur de la vue prend en paramètre le modèle
    public PersonneView(Personne model) {
        this.model = model;
    }

    public void afficher() {
        System.out.println("Nom : " + model.getNom());
        System.out.println("Prénom : " + model.getPrenom());
        System.out.println("Age : " + model.getAge());
    }
}
```

- Voici un exemple simple d'une vue ```PersonneView``` en JavaFX :

```java
package views;

import models.Personne;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class PersonneViewController implements Initializable {

    private Personne personne;

    @FXML
    private Label nomLabel;

    @FXML
    private Label prenomLabel;

    @FXML
    private Label ageLabel;

    public void setPersonne(Personne personne) {
        this.personne = personne;
        misAJourUI();
    }

    // cette méthode est appelée automatiquement par JavaFX une fois que la vue est créée
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        misAJourUI();
    }

    public void misAJourUI() {
        if(personne != null) {
            nomLabel.setText(personne.getNom());
            prenomLabel.setText(personne.getPrenom());
            ageLabel.setText(String.valueOf(personne.getAge()));
        } else {
            nomLabel.setText("");
            prenomLabel.setText("");
            ageLabel.setText("");
        }
    }
}
```

- Voici le fichier FXML associé à la vue ```PersonneView``` :

```xml
<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.geometry.Insets?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.layout.HBox?>

<HBox spacing="5" xmlns:fx="http://javafx.com/fxml/1" xmlns="http://javafx.com/javafx/17" fx:controller="views.PersonneViewController">
    <children>
        <Label fx:id="nomLabel" />
        <Label fx:id="prenomLabel" />
        <Label fx:id="ageLabel" />
    </children>
   <padding>
      <Insets bottom="5.0" left="5.0" right="5.0" top="5.0" />
   </padding>
</HBox>

```

- On voit que la vue ```PersonneView``` contient le modèle ```Personne``` en attribut. Cela permet de récupérer les données du modèle pour les afficher dans l'interface graphique. La classe ```PersonneView``` est une **contrôleur de vue** car elle contient le modèle et qu'elle gère l'affichage de ce modèle dans l'interface graphique.
- La **responsabilité** de la vue est de gérer l'affichage des données du modèle dans l'interface graphique. Elle ne doit pas contenir de code spécifique :
    - à la base de données
    - à la modification du modèle
    - au passage d'une vue à une autre

- Il est important de comprendre qu'il y a **deux types de contrôleurs** en MVC : les contrôleurs de vue et les contrôleurs d'application. Les contrôleurs de vue sont des classes qui contiennent le modèle et qui gèrent l'affichage de ce modèle dans l'interface graphique. Les contrôleurs d'application sont des classes qui vont faire le lien entre le modèle et la vue. Par exemple, un contrôleur de modèle peut gérer la sauvegarde des données du modèle dans la base de données.	
- La vue en JavaFX sera donc composée de **deux parties** : le fichier FXML et son contrôleur. Le fichier FXML contient la structure de l'interface graphique. Le contrôleur contient le modèle et gère l'affichage de ce modèle dans l'interface graphique.

###  3.3. <a name='Lecontrleurdapplication'></a>Le contrôleur (d'application)

- Voici un exemple simple d'un contrôleur ```PersonneController``` en Java :

```java
import models.Personne;
import views.PersonneViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class PersonneApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        // Création du modèle
        Personne personne = new Personne("John", "Doe", 42);

        // Création de la vue
        FXMLLoader fxmlLoader = new FXMLLoader(PersonneViewController.class.getResource("personne-view.fxml"));

        // On charge la vue
        Parent root = fxmlLoader.load();

        // On récupère le contrôleur de la vue
        PersonneViewController personneViewController = fxmlLoader.getController();

        // On passe le modèle à la vue
        personneViewController.setPersonne(personne);

        // On affiche la vue
        Scene scene = new Scene(root, 320, 240);
        stage.setTitle("Personne");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
```

- Le contrôleur ```PersonneController``` est une **contrôleur d'application** car il contient le modèle et la vue et qu'il gère les interactions entre le modèle et la vue. Il ne doit pas contenir de code spécifique à l'interface graphique (car c'est la responsabilité de la vue).
- La **responsabilité** du contrôleur d'application est uniquement de gérer les interactions entre le modèle et les vues, c'est-à-dire:
    - les interactions entre les vues
    - les demandes de modification du modèle
    - les demandes de réactions aux modifications du modèle dans la vue

- Le contrôleur d'application est donc le **point d'entrée** de l'application.

##  4. <a name='Gestiondediffrentesvues'></a>Gestion de différentes vues

- Ajoutons la possibilité de modifier les données de la personne. Pour cela, on va créer une nouvelle vue ```PersonneEditViewController``` qui va permettre de modifier les données de la personne. On va également créer un nouveau contrôleur ```PersonneEditController``` qui va gérer les interactions entre la vue ```PersonneEditViewController``` et le modèle ```Personne```.

- Voici le fichier FXML de la vue ```PersonneEditViewController``` :

```xml
<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.geometry.Insets?>
<?import javafx.scene.control.Button?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.control.TextField?>
<?import javafx.scene.layout.AnchorPane?>
<?import javafx.scene.layout.ColumnConstraints?>
<?import javafx.scene.layout.GridPane?>
<?import javafx.scene.layout.HBox?>
<?import javafx.scene.layout.RowConstraints?>

<AnchorPane xmlns="http://javafx.com/javafx/17" xmlns:fx="http://javafx.com/fxml/1" fx:controller="views.PersonneEditViewController">
   <children>
      <GridPane prefHeight="228.0" prefWidth="527.0" AnchorPane.bottomAnchor="5.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0">
        <columnConstraints>
          <ColumnConstraints hgrow="SOMETIMES" maxWidth="60.0" minWidth="60.0" prefWidth="60.0" />
          <ColumnConstraints hgrow="SOMETIMES" maxWidth="1.7976931348623157E308" minWidth="10.0" prefWidth="467.0" />
        </columnConstraints>
        <rowConstraints>
          <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
          <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
          <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
            <RowConstraints minHeight="10.0" prefHeight="30.0" vgrow="SOMETIMES" />
        </rowConstraints>
         <children>
                <Label text="Nom" />
                <TextField fx:id="nomTextField" GridPane.columnIndex="1" />
                <Label text="Prénom" GridPane.rowIndex="1" />
                <TextField fx:id="prenomTextField" GridPane.columnIndex="1" GridPane.rowIndex="1" />
                <Label text="Age" GridPane.rowIndex="2" />
                <TextField fx:id="ageTextField" GridPane.columnIndex="1" GridPane.rowIndex="2" />
              <HBox spacing="5" GridPane.columnIndex="1" GridPane.rowIndex="3">
                  <children>
                      <Button fx:id="sauverButton" text="Sauvegarder" />
                      <Button fx:id="annulerButton" text="Annuler" />
                  </children>
              </HBox>
         </children>
      </GridPane>
   </children>
   <padding>
      <Insets bottom="15.0" left="15.0" right="15.0" top="15.0" />
   </padding>
</AnchorPane>
```

- Voici le contrôleur de la vue ```PersonneEditViewController``` :

```java
package views;

import models.Personne;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class PersonneEditViewController implements Initializable {
    @FXML
    private TextField nomTextField;

    @FXML
    private TextField prenomTextField;

    @FXML
    private TextField ageTextField;

    @FXML
    private Button sauverButton;

    @FXML
    private Button annulerButton;

    private Personne personne;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        sauverButton.setOnAction(event -> sauver());
        annulerButton.setOnAction(event -> annuler());
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;

        nomTextField.setText(personne.getNom());
        prenomTextField.setText(personne.getPrenom());
        ageTextField.setText(String.valueOf(personne.getAge()));
    }

    public void sauver() {
        personne.setNom(nomTextField.getText());
        personne.setPrenom(prenomTextField.getText());
        personne.setAge(Integer.parseInt(ageTextField.getText()));
    }

    public void annuler() {

    }
}
```

- Un contrôleur de la vue ```PersonneEditViewController``` est un contrôleur de vue, mais il y a un problème: il contient du code spécifique à la gestion du modèle ```Personne```. En effet, dans la méthode ```sauver```, on voit que la vue récupère les données saisies par l'utilisateur et les passe au modèle. C'est une mauvaise pratique car la vue fait plus qu'uniquement afficher correctement les données. Dans la découpe MVC, c'est le contrôleur d'application qui doit gérer les modifications du modèle.

- Il faut donc changer la vue pour qu'elle demande au contrôleur d'application de sauver les données saisies par l'utilisateur. Pour cela, on va ajouter un **listener** de cette vue qui sur lequel on va appeler une méthode ```sauver``` en lui passant les données de l'interface. 

```java	
package controllers;

package views;

import models.Personne;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class PersonneEditViewController implements Initializable {
    @FXML
    private TextField nomTextField;

    @FXML
    private TextField prenomTextField;

    @FXML
    private TextField ageTextField;

    @FXML
    private Button sauverButton;

    @FXML
    private Button annulerButton;

    private Personne personne;
    
    // on crée un attribut listener de type PersonneEditViewListener, interface que l'on va créer en bas de ce fichier
    private PersonneEditViewListener listener;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        sauverButton.setOnAction(event -> sauver());
        annulerButton.setOnAction(event -> annuler());
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;

        nomTextField.setText(personne.getNom());
        prenomTextField.setText(personne.getPrenom());
        ageTextField.setText(String.valueOf(personne.getAge()));
    }

    public void annuler() {
        if(listener != null) {
            listener.annuler(personne);
        }

    }

    // On appelle la méthode sauver du listener en lui passant les données saisies par l'utilisateur
    // La vue ne gère plus le modèle, on délègue cette tâche à l'objet qui sera passé en paramètre de la méthode setListener
    public void sauver() {
        if(listener != null) {
            listener.sauver(personne, nomTextField.getText(), prenomTextField.getText(), Integer.parseInt(ageTextField.getText()));
        }
    }

    public void setListener(PersonneEditViewListener listener) {
        this.listener = listener;
    }

    // On crée une interface PersonneEditViewListener qui contient les méthodes sauver et annuler.
    // Cette interface doit être implémentée par l'objet qui voudra réagir aux interactions de cette vue. 
    public interface PersonneEditViewListener {
        void sauver(Personne personne, String nom, String prenom, int age);
        void annuler(Personne personne);
    }
}
```

- On a maintenant une vue qui ne gère plus le modèle ```Personne```. La classe respecte sa responsabilité de vue dans l'architecture MVC : elle ne s'occupe que de bien afficher les données et délégue le reste à d'autres objets. On peut donc la réutiliser pour d'autres modèles et d'autres manières de gérer les interactions.

- On va maintenant modifier notre contrôleur d'application qui doit gérer les interactions avec la vue ```PersonneEditViewController```.

```java
package controllers;

import models.Personne;
import views.PersonneEditViewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.io.IOException;

public class PersonneApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        Personne personne = new Personne("John", "Doe", 42);
        FXMLLoader fxmlLoader = new FXMLLoader(PersonneEditViewController.class.getResource("personne-edit-view.fxml"));
        Parent root = fxmlLoader.load();
        PersonneEditViewController personneEditViewController = fxmlLoader.getController();
        personneEditViewController.setPersonne(personne);
        personneEditViewController.setListener(new PersonneEditViewController.PersonneEditViewListener() {
            @Override
            public void sauver(Personne personne, String nom, String prenom, int age) {
                personne.setNom(nom);
                personne.setPrenom(prenom);
                personne.setAge(age);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Personne enregistrée !");
                alert.setHeaderText("C'est un succès :)");
                alert.setContentText("Les données de la personne ont été enregistrées avec succès !");
                alert.showAndWait();
            }

            @Override
            public void annuler(Personne personne) {
                // on demande de réafficher les données de la personne
                personneEditViewController.setPersonne(personne);
            }
        });
        Scene scene = new Scene(root, 320, 240);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
```

- On a donc créé une interface ```PersonneEditViewListener``` qui contient les méthodes ```sauver``` et ```annuler```. Cette interface doit être implémentée par l'objet qui voudra réagir aux interactions de cette vue. Dans notre cas, on a créé une classe anonyme qui implémente cette interface. On a donc créé un objet qui va réagir aux interactions de la vue ```PersonneEditViewController```. La classe ```PersonneApplication```, qui est un **contrôleur d'application**, qui va bien gérer maintenant les interactions avec la vue ```PersonneEditViewController``` et le changement d'états du modèle.

- Remarquez qu'on a également afficher une fenêtre d'alerte pour confirmer que les données ont bien été enregistrées. C'est bien le contrôleur d'application qui gère le déroulement de l'application et donc l'affichage des différentes fenêtres.

- Si on prend un peu de recul, on peut voir maintenant que tout le code est bien séparé :
    - La classe ```Personne``` contient les données de la personne et ne gère pas l'affichage.
    - La classe ```PersonneEditViewController``` contient le code pour afficher les données de la personne et gérer les interactions avec l'utilisateur. Mais elle délégue la gestion de ces interactions au contrôleurs d'application.
    - La classe ```PersonneApplication``` contient le code pour gérer les interactions avec la vue ```PersonneEditViewController``` et le changement d'état du modèle ```Personne```. Elle gère aussi l'affichage des fenêtres.

###  4.1. <a name='Dcouplage-1'></a>Découplage

- On peut se demander pourquoi nous avons créé une interface ```PersonneEditViewListener``` pour gérer les interactions avec la vue ```PersonneEditViewController```. Pourquoi ne pas avoir directement utiliser la classe ```PersonneApplication``` dans la classe ```PersonneEditViewController``` ?

- Si nous avions fait cela, nous aurions eu une dépendance très forte entre la vue et le contrôleur d'application. Cela aurait été un problème car la vue ne peut pas être réutilisée dans un autre contexte que celui du contrôleur d'application. La vue ne devrait pas dépendre du contrôleur d'application. La vue devrait pouvoir être réutilisée avec d'autres contrôleurs d'application.

- Pour faire cela, nous avons découplé la vue de son contrôleur d'application en passant par une interface. La vue ne dépend plus directement d'un contrôleur d'application mais juste d'une interface qui contient 2 méthodes (```sauver``` et ```annuler```). La vue ne sait pas quel objet va réagir aux interactions de l'utilisateur. La vue est donc indépendante de toute autre classe, à part du modèle.

- C'est le principe du **découplage** dont nous avons parlé plus tôt. Il permet dans notre cas de créer une vue indépendante du reste de l'application. Cela permet donc de créer des classes réutilisables et de créer des applications maintenables plus facilement.
